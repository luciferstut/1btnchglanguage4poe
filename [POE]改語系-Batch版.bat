setlocal enabledelayedexpansion
chcp 65001
@ECHO off
set poePath=C:\Program Files (x86)\Grinding Gear Games\Path of Exile\
set ggpk4ChineseFile=Content.ggpk-chinese
set ggpk4EnglishFile=Content.ggpk-english
set msg="[ERROR] Change Fail"
set catchFile=catch4Yi.txt
set target=Content.ggpk
set ggpkCnt=0
set englishCatchSize=0
set chineseCatchSize=0
set englishGGPKSize=0
set chineseGGPKSize=0
set mustReInit=
set catchContext=

REM 判斷是否為正確POE目錄
call:checkPoePath

call :getGGPKCnt ggpkCnt
ECHO "[INFO] 找到%ggpkCnt%個ggpk檔"

REM 依ggpk數量判斷是否初始化過
IF %ggpkCnt% LSS 2 (
	ECHO "[INFO] 準備進行初始化作業"
	call:initFunc
	PAUSE
	EXIT
)


call :getEnglishSize englishGGPKSize
REM ECHO "[DEBUG] englishGGPKSize:%englishGGPKSize%"

call :getChineseSize chineseGGPKSize
REM ECHO "[DEBUG] chineseGGPKSize:%chineseGGPKSize%"

cd %poePath%
REM IF EXIST "%catchFile%" (
REM 	call :getCatchSize englishCatchSize chineseCatchSize
REM 	
REM 	ECHO "[INFO] 英文ggpk大小:!englishGGPKSize!Byte, 最後切換ggpk時為:!englishCatchSize!Byte"
REM 	ECHO "[INFO] 中文ggpk大小:!chineseGGPKSize!Byte, 最後切換ggpk時為:!chineseCatchSize!Byte"
REM 
REM 	REM 為了確認ggpk是否被patch更改過，故取ggpk的檔案大小跟catch檔案內紀錄的檔案大小進行比對
REM 	call :isMustReInit mustReInit
REM 	ECHO "[INFO] 是否需要重新初始化:!mustReInit!"
REM 	REM 判斷英文ggpk是否經過patch，若patch應需初始化並中文化後，再次進行此程式
REM 	IF !mustReInit! == true (
REM 		ECHO "[WARN] 檢查ggpk大小不一致，停止切換語系"
REM 		set /p choice4Init=請問是否要初始化並刪除舊有ggpk？輸入'Y'開始初始化或輸入任意鍵退出. P.S:建議選Y待備份完成後，再次進行中文化即可		
REM 		if !choice4Init! == Y (
REM 			ECHO "[INFO] 將開始移除"
REM 			call:clearFunc
REM 			
REM 			REM 執行初始化作業
REM 			call:initFunc
REM 			
REM 			ECHO "[INFO] 初始化完成，請重新進行中文化後再次執行此程式"
REM 			PAUSE
REM 			EXIT
REM 		) ELSE (
REM 			ECHO "[INFO] 程式將不會有任何變動，請點選任一按鍵離開"
REM 			PAUSE
REM 			EXIT
REM 		)
REM 		
REM 	) ELSE (
REM 		ECHO "[INFO] 檢查ggpk大小一致，將進行切換語系"
REM 	)	
REM ) ELSE (
REM 	REM 將ggpk檔案大小保留在catch檔內
REM 	set catchContext=!englishGGPKSize!,!chineseGGPKSize!
REM 	ECHO !catchContext! > %catchFile%
REM )


IF EXIST "%poePath%%ggpk4ChineseFile%" (
	set source=%ggpk4ChineseFile%
	set backup=%ggpk4EnglishFile%
	set language=中文
) ELSE (
	set source=%ggpk4EnglishFile%
	set backup=%ggpk4ChineseFile%	
	set language=英文
)

REM 進行切換ggpk(切換語系)
cd %poePath%
ren %target% %backup%
ren %source% %target%
ECHO "[INFO] 變更完成[POE已改為>>%language%<<語系]
PAUSE
EXIT

:initFunc
cd %poePath%
IF EXIST "%poePath%%target%" (
	ECHO "┌---- [INFO] 開始複製英文ggpk檔 %target% to %ggpk4EnglishFile% ----"
	COPY /z %target% %ggpk4EnglishFile%
	ECHO "└---- [INFO] 英文ggpk檔 複製完成 %target% to %ggpk4EnglishFile% End ----"
)
ECHO "[INFO] 初始化完成，請重新進行中文化後再次執行此程式"
goto:eof

:clearFunc
cd %poePath%
IF EXIST "%poePath%%ggpk4ChineseFile%" (
	DEL %ggpk4ChineseFile%
	ECHO "[INFO] 刪除%ggpk4ChineseFile%"
)

IF EXIST "%poePath%%ggpk4EnglishFile%" (
	DEL %ggpk4EnglishFile%
	ECHO "[INFO] 刪除%ggpk4EnglishFile%"
)

IF EXIST "%poePath%%catchFile%" (
	DEL %catchFile%
	ECHO "[INFO] 刪除%catchFile%"
)
goto:eof

:checkPoePath
IF EXIST "%poePath%%target%" (
	ECHO "[INFO] 於%poePath%中找到Content.ggpk."
) ELSE (
	ECHO "[ERROR] 於%poePath%中找不到, 請使用編輯軟體打開此檔案，並將poePath參數設定為您的POE目錄."
	PAUSE
	EXIT	
)
goto:eof

:getGGPKCnt
IF EXIST "%poePath%%target%" set /a %1=%1 + 1
IF EXIST "%poePath%%ggpk4EnglishFile%" set /a %1=%1 + 1
IF EXIST "%poePath%%ggpk4ChineseFile%" set /a %1=%1 + 1
goto:eof

:getEnglishSize
cd %poePath%
IF EXIST "%ggpk4EnglishFile%" (
	FOR /F %%A in ("%ggpk4EnglishFile%") DO SET %1=%%~zA
) ELSE IF EXIST "%target%" (
	FOR /F %%A in ("%target%") DO SET %1=%%~zA
)	
goto:eof

:getChineseSize
cd %poePath%
IF EXIST "%ggpk4ChineseFile%" (
	FOR /F %%A in ("%ggpk4ChineseFile%") DO SET %1=%%~zA
) ELSE IF EXIST "%target%" (
	FOR /F %%A in ("%target%") DO SET %1=%%~zA
)	
goto:eof

:getCatchSize
cd %poePath%
for /f "tokens=1-2 delims=, " %%a in (%catchFile%) do (
	set %1=%%a
	set %2=%%b
)
goto:eof

:isMustReInit
set %1=false
IF NOT !englishGGPKSize! == !englishCatchSize! (
	set %1=true
	REM ECHO [DEBUG] 英文GGPK偵測到被Patch過.
)

IF NOT !chineseGGPKSize! == !chineseCatchSize! (
	set %1=true
	REM ECHO [DEBUG] 中文GGPK偵測到被Patch過.
)

goto:eof